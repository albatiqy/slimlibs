<?php
return [
    "handler" => Albatiqy\Slimlibs\Command\Commands\TaskRunner::class,
    "options" => [
        "help" => "Manajemen task",
        "args" => [
            ],
        "opts" => [
            ],
        "commands" => [
            "list" => [
                "help" => "Menampilkan daftar task tersedia",
                "name" => "listTasks",
                "opts" => [
                    ],
                "args" => [
                    ]
                ],
            "remap" => [
                "help" => "Remapping kelas task",
                "name" => "remapTask",
                "opts" => [
                    ],
                "args" => [
                    ]
                ],
            "bgrun" => [
                "help" => "Memproses antrian task background",
                "name" => "bgrun",
                "opts" => [
                    ],
                "args" => [
                    ]
                ],
            "run" => [
                "help" => "Memproses antrian task background",
                "name" => "run",
                "opts" => [
                    "taskname" => [
                        "name" => "taskname",
                        "help" => "nama task yang akan dieksekusi",
                        "short" => "t",
                        "arg" => "name"
                        ]
                    ],
                "args" => [
                    [
                        "arg" => "data",
                        "help" => "data berupa JSON",
                        "required" => true
                        ]
                    ]
                ]
            ]
        ]
];