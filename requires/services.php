<?php declare (strict_types = 1);

return [
    'db' => function($container) {
        $settings = $container->settings['db'];
        $db = Albatiqy\Slimlibs\Services\Database\Factory::getInstance($settings);
        return $db;
    },
    'jwt' => function($container) {
        $settings = $container->settings['jwt'];
        $jwt = new Albatiqy\Slimlibs\Services\Jwt\JWT($settings['secret'], $settings['algo'], $settings['max_age'], $settings['leeway']);
        return $jwt;
    },
    'monolog' => function($container) {
        $settings = $container->settings['monolog'];
        $logger = new Monolog\Logger($settings['name']);
        $processor = new Monolog\Processor\UidProcessor();
        $logger->pushProcessor($processor);
        $handler = new Monolog\Handler\StreamHandler($settings['path'], $settings['level']);
        $logger->pushHandler($handler);
        return $logger;
    },
    'renderer' => function($container) {
        $templates = new Albatiqy\Slimlibs\Services\Renderer\Engine(APP_DIR . '/templates');
        $templates->addFolder('libs', LIBS_DIR . '/web/templates');
        return $templates;
    },
    'validator' => function($container) {
        $messages = require APP_DIR . '/config/validation.php';
        return function($form, $labels = null, $options = []) use ($container, $messages) {
            return new Albatiqy\Slimlibs\Services\Validation\Validator($form, $options, $messages, $labels);
        };
    }
];