<?php declare (strict_types = 1);

$settings = [
    'cache_dir' => APP_DIR . '/var/cache',
    'tmp_dir' => APP_DIR . '/var/tmp',
    'log_dir' => APP_DIR . '/var/log'
];
$settings += require APP_DIR . '/config/settings.php';
if (APP_ENV == 'DEV') {
    $settings = array_replace_recursive($settings, require (APP_DIR . '/config/development.php'));
}

return $settings;