<?php declare (strict_types = 1);

return static function ($settings) {
    $services = require LIBS_DIR . '/requires/services.php';

    $container = Albatiqy\Slimlibs\Container\Container::getInstance($services);
    $container->set('settings', $settings);

    return $container;
};
