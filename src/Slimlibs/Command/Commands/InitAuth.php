<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Command\Commands;

use Albatiqy\Slimlibs\Command\AbstractCommand;
use Albatiqy\Slimlibs\Support\Helper\CodeOut;
use Albatiqy\Slimlibs\Support\Util\DocBlock;

/**
 * Inisialisasi autentikasi dan otorisasi
 *
 */

final class InitAuth extends AbstractCommand {

    public function main() {
        $dir = \APP_DIR . '/src/Actions';
        $cpos = \strlen($dir);

        $dir_iterator = new \RecursiveDirectoryIterator($dir);
        $dir_iterator->setFlags(\RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new \RecursiveIteratorIterator($dir_iterator, \RecursiveIteratorIterator::SELF_FIRST);
        $configs = [];
        foreach ($iterator as $file) {
            if ($file->isFile()) {
                $path = $file->getPath();
                $basef = '\\App\\Actions'.\str_replace('/','\\',\substr($path, $cpos).'\\'.$file->getBasename('.'.$file->getExtension()));
                //echo $basef."\n";
                $class = new \ReflectionClass($basef);
                $configs[$class->getName()] = $this->parseCommand($class);
            }
        }
        $fileout = "<?php\nreturn " . CodeOut::fromArray($configs,'') . ";";
        \file_put_contents(\APP_DIR . '/var/configs/roles.php', $fileout);
        $this->success("OK");
    }

    private function parseCommand($class) {
        $doc_block = new DocBlock($class);
        $result = [];
        $opt = $doc_block->getTagValue('authorize');
        if ($opt != null) {
            $result['role'] = $opt;
        } else {
            $opt = $doc_block->getTag('authenticate');
            if ($opt != null) {
                $result['identify'] = true;
            }
        }
        return $result;
    }
}