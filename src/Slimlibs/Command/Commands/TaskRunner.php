<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Command\Commands;

use Albatiqy\Slimlibs\Command\AbstractCommand;
use Albatiqy\Slimlibs\Command\TableFormatter;
use Albatiqy\Slimlibs\DbAccess\Tasks;
use Albatiqy\Slimlibs\Support\Helper\CodeOut;
use Albatiqy\Slimlibs\Support\Util\DocBlock;

/**
 * Manajemen task
 *
 */

final class TaskRunner extends AbstractCommand {

    /**
     * Menampilkan daftar task tersedia
     *
     * @alias [list]
     */
    public function listTasks() {
        $tf = new TableFormatter();
        $dir = \APP_DIR . '/var/tasks';
        $iterator = new \DirectoryIterator($dir);
        foreach ($iterator as $fileinfo) {
            if ($fileinfo->isFile()) {
                $cmdinfo = require $dir . '/' . $fileinfo->getBasename();
                echo $tf->format(
                    ['20%', '*'],
                    [
                        '   ' . $fileinfo->getBasename('.' . $fileinfo->getExtension()),
                        $cmdinfo['options']['desc'],
                    ],
                    ["cyan", "green"]
                );
            }
        }
    }

    /**
     * Remapping kelas task
     *
     * @alias [remap]
     */
    public function remapTask() {
        $dir = \LIBS_DIR . '/src/Slimlibs/Command/Tasks';
        $iterator = new \DirectoryIterator($dir);
        foreach ($iterator as $fileinfo) {
            if ($fileinfo->isFile()) {
                $reflect = new \ReflectionClass('\\Albatiqy\\Slimlibs\\Command\\Tasks\\' . $fileinfo->getBasename('.' . $fileinfo->getExtension()));
                $map = '';
                $result = $this->parseClass($reflect, $map);
                $fileout = "<?php\nreturn [\n    \"handler\" => " . $reflect->getName() . "::class,\n    \"options\" => " . CodeOut::fromArray($result) . "\n];";
                \file_put_contents(\APP_DIR . '/var/tasks/' . $map . '.php', $fileout);
            }
        }
        $dir = \APP_DIR . '/src/Tasks';
        $iterator = new \DirectoryIterator($dir);
        foreach ($iterator as $fileinfo) {
            if ($fileinfo->isFile()) {
                $reflect = new \ReflectionClass('\\App\\Tasks\\' . $fileinfo->getBasename('.' . $fileinfo->getExtension()));
                $map = '';
                $result = $this->parseClass($reflect, $map);
                $fileout = "<?php\nreturn [\n    \"handler\" => " . $reflect->getName() . "::class,\n    \"options\" => " . CodeOut::fromArray($result) . "\n];";
                \file_put_contents(\APP_DIR . '/var/tasks/' . $map . '.php', $fileout);
            }
        }
    }

    /**
     * Memproses antrian task background
     *
     * @alias [bgrun]
     */
    public function bgrun() { //==============logging
        $da_tasks = Tasks::getInstance();
        $tasks = $da_tasks->remains();
        foreach ($tasks as $task) {
            $fileload = \APP_DIR . '/var/tasks/' . $task->task . '.php';
            if (!\file_exists($fileload)) {
                $da_tasks->update([
                    'logs' => 'task tidak ditemukan',
                ], $task->id);
            } else {
                $cmdmanifest = require $fileload;
                $reflect = $cmdmanifest['handler'];
                $reflect = new \ReflectionClass($reflect);

                $instance = $reflect->newInstance($this->container);
                $props = $cmdmanifest['options']['props'];
                $data = \json_decode($task->data, true);
                foreach ($data as $k => $v) {
                    $property = $reflect->getProperty($props[$k]['name']);
                    $property->setValue($instance, $v);
                }
                $instance->run();
                $da_tasks->update([
                    'state' => 1,
                ], $task->id);
            }
        }
    }

    /**
     * Memproses antrian task background
     *
     * @alias [run]
     * @opt [$taskname|taksname|t|taskname] nama task yang akan dieksekusi
     * @arg [data|required] data berupa JSON
     */
    public function run($taskname = null) {
        $fileload = \APP_DIR . '/var/tasks/' . $taskname . '.php';
        if (!\file_exists($fileload)) {
            $this->error('task tidak ditemukan');
            exit;
        }
        $cmdmanifest = require $fileload;
        $reflect = $cmdmanifest['handler'];
        $reflect = new \ReflectionClass($reflect); // chk abstract command?

        $instance = $reflect->newInstance($this->container);
        $props = $cmdmanifest['options']['props'];
        $data = \json_decode($this->args[0], true);
        foreach ($data as $k => $v) {
            $property = $reflect->getProperty($props[$k]['name']);
            $property->setValue($instance, $v);
        }
        $instance->run();
    }

    public function main() {
        $this->showHelp();
    }

    private function parseClass($reflect, &$map) {
        $result = [];
        $doc_block = new DocBlock($reflect);
        $result['desc'] = $doc_block->getComment();
        $map = $doc_block->getTagValue('map');
        $properties = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);
        $props = [];
        foreach ($properties as $property) {
            $doc_block = new DocBlock($property);
            $pname = $property->getName();
            $alias = $doc_block->getTagValue('alias');
            if ($alias != null) {
                $props[$alias] = ['name' => $pname, 'desc' => $doc_block->getComment()];
            }
        }
        $result['props'] = $props;
        return $result;
    }
}