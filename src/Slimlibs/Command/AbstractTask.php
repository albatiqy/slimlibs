<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Command;

abstract class AbstractTask {

    protected $container;

    abstract public function run();

    public function __construct($container) {
        $this->container = $container;
    }
}