<?php declare (strict_types = 1);

namespace Albatiqy\Slimlibs\Support;

final class MysqlHelper {

    const TYPE_STRING = 1;
    const TYPE_NUMERIC = 2;

    private static function limit($data, $columns, $recordsFiltered, &$lastPage) {
        $limit = '';
        $lastPage = 0;
        if (isset($data['length'])) {
            $size = 10;
            if (!empty($data['length'])) {
                $size = \intval($data['length']);
            }
            if ($size < 1) {
                $size = 10;
            }
            $lastPage = \ceil($recordsFiltered / $size);
            $page = 1;
            if ($lastPage > 0) {
                if (!empty($data['page'])) {
                    $page = \intval($data['page']);
                }
                if ($page < 1) {
                    $page = 1;
                }
                if ($page > $lastPage) {
                    $page = $lastPage;
                }
            }
            $page -= 1;
            $offset = $page * $size;
            $limit = "LIMIT $offset, $size";
        } else {
            if ($recordsFiltered > 0) {
                $lastPage = 1;
            }
        }
        return $limit;
    }

    private static function order($data, $columns) {
        $order = '';
        if (!empty($data['orders'])) {
            $orderBy = [];
            foreach ($data['orders'] as $k => $sort) {
                $dir = ('asc' === $sort['dir'] ? 'ASC' : 'DESC');
                if (empty($columns[$sort['column']]['fn'])) {
                    $orderBy[] = $columns[$sort['column']]['db'] . ' ' . $dir;
                }
            }
            if (\count($orderBy)) {
                $order = 'ORDER BY ' . \implode(', ', $orderBy);
            }
        }
        return $order;
    }

    private static function filter($data, $columns, &$bindings) {
        $globalSearch = [];
        $columnSearch = [];
        if (!empty($data['search'])) {
            $str = $data['search'];
            foreach ($columns as $field => $col) {
                if (empty($col['fn']) && ($col['search'] ?? true)) {
                    if (self::TYPE_STRING == $col['type']) {
                        $binding = self::bind($bindings, '%' . $str . '%');
                        $globalSearch[] = $col['db'] . ' LIKE ' . $binding;
                    }
                }
            }
        }
        if (!empty($data['filters'])) {
            foreach ($data['filters'] as $filter) {
                if (!empty($columns[$filter['column']])) {
                    $col = $columns[$filter['column']];
                    if (empty($col['fn']) && ($col['search'] ?? true)) {
                        switch ($filter['type']) {
                        case 'like':
                            if (self::TYPE_STRING == $col['type']) {
                                $binding = self::bind($bindings, '%' . $filter['value'] . '%');
                                $columnSearch[] = $col['db'] . ' LIKE ' . $binding;
                            }
                            break;
                        case '=':
                        case '!=':
                            $binding = self::bind($bindings, $filter['value']);
                            $columnSearch[] = $col['db'] . ' = ' . $binding;
                            break;
                        case '<':
                        case '<=':
                        case '>':
                        case '>=':
                            if (self::TYPE_NUMERIC == $col['type'] && \is_numeric($filter['value'])) {
                                $binding = self::bind($bindings, '%' . $filter['value'] . '%');
                                $columnSearch[] = $col['db'] . ' LIKE ' . $binding;
                            }
                            break;
                            // in type??
                        }
                    }
                }
            }
        }
        $where = '';
        if (\count($globalSearch)) {
            $where = '(' . \implode(' OR ', $globalSearch) . ')';
        }
        if (\count($columnSearch)) {
            $where = '' === $where ? \implode(' AND ', $columnSearch) : $where . ' AND ' . \implode(' AND ', $columnSearch);
        }
        if ('' !== $where) {
            $where = 'WHERE ' . $where;
        }
        return $where;
    }

    public static function simple($db, $data, $table, $primaryKey, $columns) {
        $bindings = [];
        $where = self::filter($data, $columns, $bindings);
        $resFilterLength = self::sql_exec($db, $bindings, "SELECT COUNT({$primaryKey}) AS a FROM $table $where");
        $recordsFiltered = (int) $resFilterLength[0]->a;
        $lastPage = 1;
        $limit = self::limit($data, $columns, $recordsFiltered, $lastPage);
        $order = self::order($data, $columns);
        $sql = 'SELECT ' . self::selects($columns) . " FROM $table $where $order $limit";
        $rows = self::sql_exec($db, $bindings, $sql);
        $resTotalLength = self::sql_exec($db, "SELECT COUNT({$primaryKey}) AS a FROM $table");
        $recordsTotal = (int) $resTotalLength[0]->a;
        return (object)['pageCount' => $lastPage, 'recordsFiltered' => $recordsFiltered, 'recordsTotal' => $recordsTotal, 'data' => $rows]; //current_page
    }

    public static function complex($db, $data, $table, $primaryKey, $columns, $whereResult = null, $whereAll = null) {
        $bindings = [];
        $localWhereResult = [];
        $localWhereAll = [];
        $whereAllSql = '';
        $where = self::filter($data, $columns, $bindings);
        $whereResult = self::_flatten($whereResult);
        $whereAll = self::_flatten($whereAll);
        if ($whereResult) {
            $where = $where ? $where . ' AND ' . $whereResult : 'WHERE ' . $whereResult;
        }
        if ($whereAll) {
            $where = $where ? $where . ' AND ' . $whereAll : 'WHERE ' . $whereAll;
            $whereAllSql = 'WHERE ' . $whereAll;
        }
        $resFilterLength = self::sql_exec($db, $bindings, "SELECT COUNT({$primaryKey}) AS a FROM $table $where");
        $recordsFiltered = (int) $resFilterLength[0]->a;
        $lastPage = 1;
        $limit = self::limit($data, $columns, $recordsFiltered, $lastPage);
        $order = self::order($data, $columns);
        $rows = self::sql_exec($db, $bindings, 'SELECT ' . self::selects($columns) . " FROM $table $where $order $limit");
        $resTotalLength = self::sql_exec($db, $bindings, "SELECT COUNT({$primaryKey}) AS a FROM $table " . $whereAllSql);
        $recordsTotal = (int) $resTotalLength[0]->a;
        return (object)['pageCount' => $lastPage, 'recordsFiltered' => $recordsFiltered, 'recordsTotal' => $recordsTotal, 'data' => $rows];
    }

    private static function sql_exec($db, $bindings, $sql = null) {
        if (null === $sql) {
            $sql = $bindings;
        }
        $stmt = $db->prepare($sql);
        if (\is_array($bindings)) {
            for ($i = 0, $ien = \count($bindings); $i < $ien; $i++) {
                $binding = $bindings[$i];
                $stmt->bindValue($binding['key'], $binding['val']);
            }
        }
        try {
            $stmt->execute();
        } catch (\PDOException $e) {
            self::handlePDOException($e);
        }
        return $stmt->fetchAll();
    }

    private static function handlePDOException($e) {
        $msg = $e->getMessage();
        throw new \Exception($msg);
    }

    private static function bind(&$a, $val) {
        $key = ':binding_' . \count($a);
        $a[] = ['key' => $key, 'val' => $val];
        return $key;
    }

    private static function selects($columns) {
        $out = [];
        foreach ($columns as $field => $col) {
            if (!($col['hidden'] ?? false)) {
                if (!empty($col['fn'])) {
                    if (\is_array($col['fn'])) {
                        $sfn = $col['db'];
                        foreach ($col['fn'] as $fn) {
                            $sfn = "$fn($sfn)";
                        }
                        $out[] = $sfn . ' AS ' . $field;
                    } else {
                        $out[] = \sprintf($col['fn'], $col['db']) . ' AS ' . $field;
                    }
                } else {
                    $out[] = $col['db'] . ' AS ' . $field;
                }
            }
        }
        return \implode(', ', $out);
    }

    private static function _flatten($a, $join = ' AND ') {
        if (!$a) {
            return '';
        } else if ($a && \is_array($a)) {
            return \implode($join, $a);
        }
        return $a;
    }

    public static function insert($db, $table, $data, Array $extras = []) {
        if (\count($extras) > 0) {
            $data = \array_merge($data, $extras);
        }
        $cols1 = $cols2 = '';
        $binds = self::bindSql($data, $cols1, $cols2);
        $sql = "INSERT INTO $table (" . $cols1 . ') VALUES (' . $cols2 . ')';
        $stmt = $db->prepare($sql);
        self::executeStmt($stmt, $binds);
        return $data;
    }

    public static function update($db, $table, $data, $primaryKey, $pkUpd = '') {
        $cols1 = $cols2 = null;
        $cols3 = '';
        $binds = self::bindSql($data, $cols1, $cols2, $cols3, ($pkUpd == '' ? [] : [$primaryKey]));
        $pkPrms = $primaryKey;
        if ('' !== $pkUpd) {
            $pkPrms = 'old_' . $primaryKey;
            $binds[':' . $pkPrms] = $pkUpd;
        }
        $sql = "UPDATE $table SET " . $cols3 . ' WHERE ' . $primaryKey . '=:' . $pkPrms;
        $stmt = $db->prepare($sql);
        self::executeStmt($stmt, $binds);
        return $data;
    }

    public static function delete($db, $table, $primaryKey, $data) {
        $sql = 'DELETE FROM ' . $table . ' WHERE ' . $primaryKey . '=:' . $primaryKey;
        $stmt = $db->prepare($sql);
        self::executeStmt($stmt, [
            ':' . $primaryKey => $data[$primaryKey],
        ]);
    }

    public static function massInsert($db, $table, $data, $ids, Callable $callable, $extras = []) {
        $repeat = $data['ids'];
        unset($data['ids']);
        $data[$ids] = null;
        if (\count($extras)) {
            $extras = \array_fill_keys($extras, null);
            $data = \array_merge($data, $extras);
        }
        $keys = \array_keys($data); //mungkin ada yg simpel
        $sql = "INSERT INTO $table (" . \implode(', ', $keys) . ') VALUES (' . \implode(', ', \array_map(function ($val) {return ':' . $val;}, $keys)) . ')';
        $stmt = $db->prepare($sql);
        try {
            foreach ($repeat as $index => $r) {
                $data[$ids] = $r;
                $binds = self::bindSql($callable($data, $index));
                $stmt->execute($binds);
            }
        } catch (\PDOException $e) {
            self::handlePDOException($e);
        }
    }

    public static function massDelete($db, $table, $primaryKey, $data) {
        $sql = 'DELETE FROM ' . $table . ' WHERE ' . $primaryKey . '=:' . $primaryKey;
        $stmt = $db->prepare($sql);
        try {
            foreach ($data['ids'] as $id) {
                $stmt->execute([':' . $primaryKey => $id]);
            }
        } catch (\PDOException $e) {
            self::handlePDOException($e);
        }
    }

    private static function bindSql($data, &$cols1 = null, &$cols2 = null, &$cols3 = null, $strips = []) { //add strip some column
        $binds = [];
        if (\count($strips) > 0) {
            foreach ($strips as $vkey) {
                unset($data[$vkey]);
            }
        }
        foreach ($data as $key => $input) {
            $binds[':' . $key] = $input;
        }
        if ($cols1 !== null || $cols3 !== null) {
            $keys = \array_keys($data);
            if (null !== $cols2) {
                $cols1 = \implode(', ', $keys);
                $cols2 = \implode(', ', \array_map(function ($val) {return ':' . $val;}, $keys));
            }
            if (null !== $cols3) {
                $cols3 = \implode(', ', \array_map(function ($val) {return $val . '=:' . $val;}, $keys));
            }
        }
        return $binds;
    }

    public static function insertUpdate($db, $table, $keys, $data) {
        $cols1 = $cols2 = '';
        $cols3 = null;
        $binds = self::bindSql($data, $cols1, $cols2, $cols3);
        foreach ($keys as $index) {
            $binds[':' . $index . '1'] = $binds[':' . $index];
        }
        $cols4 = \implode(', ', \array_map(function ($val) {return $val . '=:' . $val . '1';}, $keys));
        $sql = 'INSERT INTO ' . $table . ' (' . $cols1 . ') VALUES (' . $cols2 . ') ';
        $sql .= 'ON DUPLICATE KEY UPDATE ' . $cols4;
        $stmt = $db->prepare($sql);
        self::executeStmt($stmt, $binds);
        return $data;
    }

    public static function queryById($db, $table, $columns, $primaryKey, $data, $extrawhere = '') {
        $dotpos = \strpos($primaryKey, '.');
        $id = (false !== $dotpos ? \substr($primaryKey, $dotpos + 1) : $primaryKey);
        $sql = 'SELECT ' . $columns . '  FROM ' . $table . ' WHERE ' . $primaryKey . '=:' . $id . ($extrawhere ? ' ' . $extrawhere : '');
        $stmt = $db->prepare($sql);
        self::executeStmt($stmt, [':' . $id => (\is_array($data) ? $data[$id] : $data) ?? null]);
        return $stmt;
    }

    private static function executeStmt($stmt, $binds) {
        try {
            $stmt->execute($binds);
        } catch (\PDOException $e) {
            self::handlePDOException($e);
        }
    }

    private static function logStmt($stmt) {
        \ob_start();
        $stmt->debugDumpParams();
        xC('logger')->info(\ob_get_clean());
    }

    private static function log($msg) {
        xC('logger')->info($msg);
    }
}