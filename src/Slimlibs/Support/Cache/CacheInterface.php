<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Cache;

interface CacheHandler {
    public function cacheRetrieve($key);
}