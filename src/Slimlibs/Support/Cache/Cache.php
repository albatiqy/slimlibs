<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Cache;

final class Cache {

    protected static $instace = [];

    public static function instance($class) {
        if (self::$instance[$class]==null) {
            self::$instance[$class] = new self();
        }
        return self::$instance[$class];
    }

}