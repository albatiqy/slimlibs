<?php declare (strict_types = 1);

namespace Albatiqy\Slimlibs\Error;

use Psr\Container\ContainerInterface;
use Slim\Error\Renderers\HtmlErrorRenderer as ParentHtmlErrorRenderer;
use Slim\Exception\HttpException;
use Throwable;

class HtmlErrorRenderer extends ParentHtmlErrorRenderer {

    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function __invoke(Throwable $exception, bool $displayErrorDetails): string {
        if ($exception instanceof HttpException) {
            $plates = $this->container->renderer;
            $request = $exception->getRequest();
            $accepts = \explode(',',$request->getHeader('Accept')[0]);
            if (\count($accepts) > 1) {
                if ($displayErrorDetails) {
                    if (\file_exists(\APP_DIR.'/templates/mpa/system/http_error_detail.php')) {
                        return $plates->make('mpa/system/http_error_detail')->render(['exception' => $exception]);
                    }
                    return $this->renderExceptionFragment($exception);
                } else {
                    return $plates->make('mpa/system/http_error')->render(['exception' => $exception]);
                }
            } else {
                if ($displayErrorDetails) {
                    if (\file_exists(\APP_DIR.'/templates/spa/system/http_error_detail.php')) {
                        return $plates->make('spa/system/http_error_detail')->render(['exception' => $exception]);
                    }
                    return $this->renderExceptionFragment($exception);
                } else {
                    return $plates->make('spa/system/http_error')->render(['exception' => $exception]);
                }
            }
        }
        return parent::__invoke($exception, $displayErrorDetails);
    }

    private function renderExceptionFragment(Throwable $exception): string {
        $html = \sprintf('<div><strong>Type:</strong> %s</div>', \get_class($exception));

        $code = $exception->getCode();
        if ($code !== null) {
            $html .= \sprintf('<div><strong>Code:</strong> %s</div>', $code);
        }

        $message = $exception->getMessage();
        if ($message !== null) {
            $html .= \sprintf('<div><strong>Message:</strong> %s</div>', \htmlentities($message));
        }

        $file = $exception->getFile();
        if ($file !== null) {
            $html .= \sprintf('<div><strong>File:</strong> %s</div>', $file);
        }

        $line = $exception->getLine();
        if ($line !== null) {
            $html .= \sprintf('<div><strong>Line:</strong> %s</div>', $line);
        }

        $trace = $exception->getTraceAsString();
        if ($trace !== null) {
            $html .= '<h2>Trace</h2>';
            $html .= \sprintf('<pre>%s</pre>', \htmlentities($trace));
        }

        return $html;
    }
}
