<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Actions;

use Psr\Container\ContainerInterface;
use Albatiqy\Slimlibs\Result\Exception;
use Slim\Exception\HttpInternalServerErrorException;

abstract class ResultAction {

    protected $container;
    protected $request = null;
    protected $response = null;

    abstract protected function getResult(array $data, array $args);

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function __invoke($request, $response, $args) {
        $this->request = $request;
        $this->response = $response;
        $data = $request->getParsedBody() ?? [];
        $data += $request->getQueryParams();

        $result = $this->getResult($data, $args);

        $status = $result->getStatus();
        $this->response = $this->response->withStatus($status);
        if ($status!=204) {
            $payload = \json_encode($result);
            $this->response->getBody()->write($payload);
            $this->response = $this->response
            ->withHeader('Content-Type', 'application/json');
        }
        return $this->response;
    }

    public function __get($key) {
        return $this->container->$key;
    }

    protected function sendNotAuthorized($message = '') {
        throw new Exception\UnauthorizedException($this->request, [], $message);
    }

    protected function sendValidationError(array $errors) {
        throw new Exception\ValidationException($this->request, $errors);
    }

    protected function sendNotExist($message = '') {
        throw new Exception\NotExistException($this->request, [], $message);
    }

    protected function sendServerError($message = '') {
        throw new HttpInternalServerErrorException($this->request, $message);
    }
}
