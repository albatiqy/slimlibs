<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Actions;

use Psr\Container\ContainerInterface;

abstract class ViewAction {

    protected $container;
    protected $request = null;
    protected $response = null;
    protected $args = [];
    protected $data = [];

    abstract protected function getResponse(array $args);

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function __invoke($request, $response, $args) {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;
        return $this->getResponse($args);
    }

    protected function render($template) {
        $plates = $this->renderer;
        //$settings = $this->settings;
        $plates->addData(['APP' => [
            'container'=>$this->container
        ]]); //global
        $this->data['args'] = $this->args; //local
        $output = $plates->make($template)->render($this->data);
        $this->response->getBody()->write($output);
        return $this->response;
    }

    public function __get($key) {
        return $this->container->$key;
    }
}