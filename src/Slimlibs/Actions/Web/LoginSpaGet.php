<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Actions\Web;

use Albatiqy\Slimlibs\Actions\ViewAction;

final class LoginSpaGet extends ViewAction {

    protected function getResponse(array $args) {
        return $this->render('spa/system/login');
    }
}
