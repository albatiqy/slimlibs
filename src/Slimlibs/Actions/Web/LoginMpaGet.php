<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Actions\Web;

use Albatiqy\Slimlibs\Actions\ViewAction;
use App\DbAccess\Users;

final class LoginMpaGet extends ViewAction {

    protected function getResponse(array $args) {
        $da_users = Users::getInstance();
        $this->data['labels'] = $da_users->getLabels();
        return $this->render('mpa/system/login');
    }
}
