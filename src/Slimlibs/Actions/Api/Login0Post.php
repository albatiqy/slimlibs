<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Actions\Api;

use Albatiqy\Slimlibs\Actions\ResultAction;
use Albatiqy\Slimlibs\Result\Results\Data;

final class Login0Post extends ResultAction {

    protected function getResult(array $data, array $args) {

        $email = (string) ($data['email'] ?? '');
        $password = (string) ($data['password'] ?? '');

        $isValidLogin = ($email === 'fauzi.ahmad@gmail.com' && $password === 'secret');

        if (!$isValidLogin) {
            $this->sendNotAuthorized();
        }

        $jwt = $this->jwt;
        $jwt_settings = ($this->settings)['jwt'];
        $time = \time();
        $expires = $time + $jwt_settings['max_age'];
        $jwt->setTestTimestamp($time);
        $token = $jwt->encode([
            'uid'    => 1,
            'aud'    => $jwt_settings['aud'],
            'scopes' => ['user'],
            'iss'    => $jwt_settings['iss']
        ]);

        return new Data([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => $expires,
        ]);
    }
}
