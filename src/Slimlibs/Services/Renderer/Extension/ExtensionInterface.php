<?php
namespace Albatiqy\Slimlibs\Services\Renderer\Extension;

use Albatiqy\Slimlibs\Services\Renderer\Engine;

/**
 * A common interface for extensions.
 */
interface ExtensionInterface
{
    public function register(Engine $engine);
}
