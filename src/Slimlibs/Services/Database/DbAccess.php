<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Services\Database;

use Albatiqy\Slimlibs\Container\Container;
use Albatiqy\Slimlibs\Services\Validation\ValidationException;
use Albatiqy\Slimlibs\Support\Util\Labels;

abstract class DbAccess {

    const TYPE_STRING = 1;
    const TYPE_NUMERIC = 2;

    protected const RULE_CREATE = 1;
    protected const RULE_UPDATE = 2;
    protected const RULE_DELETE = 3;

    protected const TABLE_NAME = null;
    protected const PRIMARY_KEY = null;
    protected const ENTITY_NAME = null;
    protected const COLUMN_DEFS = [];
    protected const AUTO_ID = true;

    private static $instances = [];
    private static $dbs = [];

    private $labels = null;

    protected $container = null;
    protected $unsetAttribs = [];

    abstract protected function getRules($opr);

    private function __construct($container) {
        $this->container = $container;
    }

    public static function getInstance() {
        $class = \get_called_class();
        if (!isset(self::$instances[$class])) {
            self::$instances[$class] = new static(Container::getInstance());
        }
        return self::$instances[$class];
    }

    public function getLabels() {
        if ($this->labels==null) {
            $this->labels = Labels::getInstance(static::ENTITY_NAME);
        }
        return $this->labels;
    }

    protected function db($key = 'default') {
        if (!isset(self::$dbs[$key])) {
            self::$dbs[$key] = ($this->container->db)($key);
        }
        return self::$dbs[$key];
    }

    protected static function defCol($attrib) {
        return static::COLUMN_DEFS[$attrib]['db'];
    }

    protected function throwValidationException(array $errors = []) {
        throw new ValidationException($errors);
    }

    protected function throwRecordNotFound() {
        throw new DbAccessException("record not found", DbAccessException::E_NO_RESULT);
    }

    protected function throwPDOException($exception) {
        throw new DbAccessException($exception->getMessage(), DbAccessException::E_PDO, $exception);
    }

    protected function unsetData($data) {
        foreach ($this->unsetAttribs as $list) {
            unset($data[$list]);
        }
        return $data;
    }
}