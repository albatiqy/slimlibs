<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Services\Database;

abstract class MySqlDbAccess extends DbAccess {

    private static $strippedCols = [];
    private static $strippedTbl = null;

    protected static function tableX() {
        if (self::$strippedTbl==null) {
            $dotpos = \strpos(static::TABLE_NAME, ' ');
            self::$strippedTbl = (false !== $dotpos ? \substr(static::TABLE_NAME, 0, $dotpos) : static::TABLE_NAME);
        }
        return self::$strippedTbl;
    }

    protected static function primaryKeyDef() {
        return self::defCol(static::PRIMARY_KEY);
    }

    protected static function primaryKeyDefX() {
        return self::defColX(static::PRIMARY_KEY);
    }

    protected function getLastIdTbl($db) {
        if (!$db->inTransaction()) {
            throw new \Exception(__METHOD__." require active transaction");
        }
        $primaryKey = self::primaryKeyDef();
        $table = static::TABLE_NAME;
        $sql = "select $primaryKey pk01 from $table order by $primaryKey desc limit 0,1";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        if (\is_object($row)) {
            return $row->pk01;
        }
        return null;
    }

    protected static function defColX($attrib) {
        if (!isset(self::$strippedCols[$attrib])) {
            $col = static::COLUMN_DEFS[$attrib]['db'];
            $dotpos = \strpos($col, '.');
            self::$strippedCols[$attrib] = (false !== $dotpos ? \substr($col, $dotpos + 1) : $col);
        }
        return self::$strippedCols[$attrib];
    }

    public function getById($id, $columns = []) {
        $row = $this->findById($id, $columns);
        if (!$row) {
            $this->throwRecordNotFound();
        }
        return $row;
    }

    public function findById($id, $attribs = [], $extrawhere = '') {
        $db = $this->db();
        $columns = self::selects($attribs);
        $idcol = static::PRIMARY_KEY;
        $primaryKey = self::primaryKeyDef();
        $table = static::TABLE_NAME;
        $sql = "SELECT $columns FROM $table WHERE $primaryKey = :$idcol" . ($extrawhere ? " $extrawhere" : '');
        $stmt = $db->prepare($sql);
        $stmt->execute([':' . $idcol => $id]);
        return $stmt->fetch();
    }

    public function findAll($params, $attribs = [], $whereResult = null, $whereAll = null) {
        $db = $this->db();
        $table = static::TABLE_NAME;
        $primaryKey = self::primaryKeyDef();
        $bindings = [];
        $localWhereResult = [];
        $localWhereAll = [];
        $whereAllSql = '';
        $where = self::filter($params, $bindings);
        $whereResult = self::flatten($whereResult);
        $whereAll = self::flatten($whereAll);
        if ($whereResult) {
            $where = $where ? "$where AND $whereResult" : "WHERE $whereResult";
        }
        if ($whereAll) {
            $where = $where ? "$where AND $whereAll" : "WHERE $whereAll";
            $whereAllSql = "WHERE $whereAll";
        }
        $resFilterLength = self::sql_exec($db, $bindings, "SELECT COUNT($primaryKey) AS aa1 FROM $table $where");
        $recordsFiltered = (int) $resFilterLength[0]->aa1;
        $lastPage = 1;
        $limit = self::limit($params, $recordsFiltered, $lastPage);
        $order = self::order($params);
        $rows = self::sql_exec($db, $bindings, 'SELECT ' . self::selects() . " FROM $table $where $order $limit");
        $resTotalLength = self::sql_exec($db, $bindings, "SELECT COUNT($primaryKey) AS aa1 FROM $table $whereAllSql");
        $recordsTotal = (int) $resTotalLength[0]->aa1;
        return (object) ['pageCount' => $lastPage, 'recordsFiltered' => $recordsFiltered, 'recordsTotal' => $recordsTotal, 'data' => $rows];
    }

    protected function getLastIdSeq($db) {
        if (!$db->inTransaction()) {
            throw new DbAccessException(__METHOD__." require active transaction", DbAccessException::E_ANY);
        }
        $primaryKey = self::primaryKeyDefX();
        $sql = 'select last_val from sys_pk_sequences where pk_name=:pk_name';
        $stmt = $db->prepare($sql);
        //\file_put_contents(\APP_DIR.'/var/text1.txt', $primaryKey);
        $stmt->execute([':pk_name'=>$primaryKey]);
        $row = $stmt->fetch();
        if (\is_object($row)) {
            return $row->last_val;
        }
        return null;
    }

    public function create($data) {
        $db = $this->db();
        $validator = ($this->container->validator)($this->getRules(self::RULE_CREATE), $this->getLabels());
        if ($validator->validate($data)) {
            $data = $validator->getValues();
            $new_id = null;
            $data = $this->unsetData($data);
            if (static::AUTO_ID) {
                $stmt = $this->insert($db, $data);
                $new_id = $db->lastInsertId();
            } else {
                try {
                    $db->beginTransaction();
                    $stmt = $this->insert($db, $data);
                    $new_id = $db->getLastIdSeq($db);
                    $db->commit();
                } catch (\Exception $e) {
                    $db->rollBack();
                    if ($e instanceof \PDOException) {
                        $this->throwPDOException($e);
                    }
                    throw new DbAccessException($e->getMessage(), DbAccessException::E_ANY);
                }
            }
            return $this->findById($new_id);
        } else {
            $errors = $validator->getErrors();
            $this->throwValidationException($errors);
        }
    }

    protected function insert($db, $data) {
        $cols1 = $cols2 = '';
        $binds = self::bindSql($data, $cols1, $cols2);
        $table = self::tableX();
        $sql = "INSERT INTO $table ($cols1) VALUES ($cols2)";
        $stmt = $db->prepare($sql);
        $stmt->execute($binds);
        return $stmt;
    }

    public function update($data, $id, $pkUpd = '') {
        $db = $this->db();
        $data[static::PRIMARY_KEY] = $id;
        $validator = ($this->container->validator)($this->getRules(self::RULE_UPDATE));
        if ($validator->validate($data)) {
            $data = $validator->getValues();
            $data = $this->unsetData($data);

            $idcol = static::PRIMARY_KEY;
            $cols1 = $cols2 = null;
            $cols3 = '';
            $binds = self::bindSql($data, $cols1, $cols2, $cols3, ($pkUpd == '' ? [] : [$idcol]));
            $pkPrms = $idcol;
            if ('' !== $pkUpd) {
                $pkPrms = "old_$idcol";
                $binds[":$pkPrms"] = $pkUpd;
            }
            $table = self::tableX();
            $sql = "UPDATE $table SET $cols3 WHERE $idcol = :$pkPrms";
            $stmt = $db->prepare($sql);
            //$stmt->debugDumpParams();
            $stmt->execute($binds); // check result
            return $this->getById($id);
        } else {
            $errors = $validator->getErrors();
            $this->throwValidationException($errors);
        }
    }

    public function delete($id) {
        $db = $this->db();
        $idcol = static::PRIMARY_KEY;
        $table = self::tableX();
        $primaryKey = self::primaryKeyDefX();
        $sql = "DELETE FROM $table WHERE $primaryKey = :$idcol";
        $stmt = $db->prepare($sql);
        $stmt->execute([
            ":$idcol" => $id,
        ]);
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            $this->throwRecordNotFound();
        }
    }

    private static function selects($attribs = []) {
        $out = [];
        foreach (static::COLUMN_DEFS as $attrib => $col) {
            if (!($col['hidden'] ?? false)) {
                if (!empty($col['fn'])) {
                    if (\is_array($col['fn'])) {
                        $sfn = $col['db'];
                        foreach ($col['fn'] as $fn) {
                            $sfn = "$fn($sfn)";
                        }
                        $out[] = "$sfn AS $attrib";
                    } else {
                        $out[] = \sprintf($col['fn'], $col['db']) . " AS $attrib";
                    }
                } else {
                    $out[] = "{$col['db']} AS $attrib";
                }
            }
        }
        return \implode(', ', $out);
    }

    private static function filter($params, &$bindings) {
        $globalSearch = [];
        $columnSearch = [];
        if (!empty($params['search'])) {
            $str = $params['search'];
            foreach (static::COLUMN_DEFS as $attrib => $col) {
                if (empty($col['fn']) && ($col['search'] ?? true)) {
                    if (self::TYPE_STRING == $col['type']) {
                        $binding = self::bind($bindings, "%$str%");
                        $globalSearch[] = "{$col['db']} LIKE $binding";
                    }
                }
            }
        }
        if (!empty($params['filters'])) {
            foreach ($params['filters'] as $filter) {
                if (!empty(static::COLUMN_DEFS[$filter['column']])) {
                    $col = static::COLUMN_DEFS[$filter['column']];
                    if (empty($col['fn']) && ($col['search'] ?? true)) {
                        switch ($filter['type']) {
                        case 'like':
                            if (self::TYPE_STRING == $col['type']) {
                                $binding = self::bind($bindings, "%{$filter['value']}%");
                                $columnSearch[] = "{$col['db']} LIKE $binding";
                            }
                            break;
                        case '=':
                        case '!=':
                            $binding = self::bind($bindings, $filter['value']);
                            $columnSearch[] = "{$col['db']} = $binding";
                            break;
                        case '<':
                        case '<=':
                        case '>':
                        case '>=':
                            if (self::TYPE_NUMERIC == $col['type'] && \is_numeric($filter['value'])) {
                                $binding = self::bind($bindings, "%{$filter['value']}%");
                                $columnSearch[] = "{$col['db']} LIKE $binding";
                            }
                            break;
                            // in type??
                        }
                    }
                }
            }
        }
        $where = '';
        if (\count($globalSearch)) {
            $where = '(' . \implode(' OR ', $globalSearch) . ')';
        }
        if (\count($columnSearch)) {
            $where = '' === $where ? \implode(' AND ', $columnSearch) : "$where AND " . \implode(' AND ', $columnSearch);
        }
        if ('' !== $where) {
            $where = "WHERE $where";
        }
        return $where;
    }

    private static function limit($params, $recordsFiltered, &$lastPage) {
        $limit = '';
        $lastPage = 0;
        if (isset($params['length'])) {
            $size = 10;
            if (!empty($params['length'])) {
                $size = \intval($params['length']);
            }
            if ($size < 1) {
                $size = 10;
            }
            $lastPage = \ceil($recordsFiltered / $size);
            $page = 1;
            if ($lastPage > 0) {
                if (!empty($params['page'])) {
                    $page = \intval($params['page']);
                }
                if ($page < 1) {
                    $page = 1;
                }
                if ($page > $lastPage) {
                    $page = $lastPage;
                }
            }
            $page -= 1;
            $offset = $page * $size;
            $limit = "LIMIT $offset, $size";
        } else {
            if ($recordsFiltered > 0) {
                $lastPage = 1;
            }
        }
        return $limit;
    }

    private static function order($params) {
        $order = '';
        if (!empty($params['orders'])) {
            $orderBy = [];
            foreach ($params['orders'] as $k => $sort) {
                $dir = ('asc' === $sort['dir'] ? 'ASC' : 'DESC');
                if (empty(static::COLUMN_DEFS[$sort['column']]['fn'])) {
                    $orderBy[] = static::COLUMN_DEFS[$sort['column']]['db'] . ' ' . $dir;
                }
            }
            if (\count($orderBy)) {
                $order = 'ORDER BY ' . \implode(', ', $orderBy);
            }
        }
        return $order;
    }

    private static function sql_exec($db, $bindings, $sql = null) {
        if (null === $sql) {
            $sql = $bindings;
        }
        $stmt = $db->prepare($sql);
        if (\is_array($bindings)) {
            foreach ($bindings as $key=>$binding) {
                $stmt->bindValue($key, $binding);
            }

            /*
            for ($i = 0, $ien = \count($bindings); $i < $ien; $i++) {
                $binding = $bindings[$i];
                $stmt->bindValue($binding['key'], $binding['val']);
            }
            */
        }
        $stmt->execute();
        return $stmt->fetchAll();
    }

    private static function bind(&$a, $val) {
        $key = ':binding_' . \count($a);
        /*
        $a[] = ['key' => $key, 'val' => $val];
        */
        $a[$key] = $val;
        return $key;
    }

    private static function flatten($a, $join = ' AND ') {
        if (!$a) {
            return '';
        } else if ($a && \is_array($a)) {
            return \implode($join, $a);
        }
        return $a;
    }

    private static function bindSql($data, &$cols1 = null, &$cols2 = null, &$cols3 = null, $strips = []) { //add strip some column
        $binds = [];
        if (\count($strips) > 0) {
            foreach ($strips as $vkey) {
                unset($data[$vkey]);
            }
        }
        foreach ($data as $key => $input) {
            $binds[":$key"] = $input;
        }
        if ($cols1 !== null || $cols3 !== null) {
            $keys = \array_keys($data);
            if (null !== $cols2) {
                $cols1 = \implode(', ', \array_map(function ($val) {return self::defColX($val);}, $keys));
                $cols2 = \implode(', ', \array_map(function ($val) {return ":$val";}, $keys));
            }
            if (null !== $cols3) {
                $cols3 = \implode(', ', \array_map(function ($val) {return "$val = :$val";}, $keys));
            }
        }
        return $binds;
    }
}