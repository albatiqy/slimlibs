<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Container;

use Closure;
use Exception;
use Psr\Container\ContainerInterface;
use ReflectionClass;

class Container implements ContainerInterface {

    private static $instance = null;
    private $registers = [];
    private $services = [];
    private $closures = [];

    private function __construct(array $default) {
        $this->registers += $default;
    }

    public function get($id) {
        if (isset($this->services[$id])) {
            return $this->services[$id];
        }
        if (\class_exists($id)) {
            $refl = new ReflectionClass($id);
            if ($refl->isInstantiable()) {
                $this->services[$id] = $this->instance($refl);
                return $this->services[$id];
            }
        }
        throw new NotFoundException();
    }

    public function __get($id) { //singleton
        if (isset($this->services[$id])) {
            return $this->services[$id];
        }
        $item = $this->resolve($id);
        if ($item instanceof ReflectionClass) {
            if ($item->isInstantiable()) {
                $this->services[$id] = $this->instance($item);
                unset($this->registers[$id]);
                return $this->services[$id];
            } else {
                unset($this->registers[$id]);
            }
        } else {
            return $item;
        }
        throw new NotFoundException();
    }

    public function __invoke($id) { //new instance
        if (!isset($this->closures[$id])) {
            $item = $this->resolve($id);
            if ($item instanceof ReflectionClass) {
                if ($item->isInstantiable()) {
                    return $this->instance($item);
                }
            }
            return $item;
        } else {
            $name = $this->closures[$id];
            return $name($this);
        }
        throw new NotFoundException();
    }

    public function set(string $key, $value) {
        $this->services[$key] = $value;
        return $this;
    }

    public function has($id) {
        if (isset($this->services[$id])) {
            return true;
        }
        $item = null;
        try {
            $item = $this->resolve($id);
        } catch (NotFoundException $e) {
            return false;
        }
        if ($item instanceof ReflectionClass) {
            return $item->isInstantiable();
        }
        return false;
    }

    private function resolve($id) {
        try {
            if (isset($this->registers[$id])) {
                if ($this->registers[$id] instanceof Closure) {
                    $name = $this->registers[$id];
                    $this->services[$id] = $name($this);
                    $this->services[\get_class($this->services[$id])] = $this->services[$id];
                    $this->closures[$id] = $this->registers[$id];
                    unset($this->registers[$id]);
                    return $this->services[$id];
                } elseif (\class_exists($id)) {
                    return new ReflectionClass($id);
                }
            }
            throw new Exception();
        } catch (Exception $e) {
            throw new NotFoundException($e->getMessage(), $e->getCode(), $e);
        }
    }

    private function instance(ReflectionClass $item) {
        $constructor = $item->getConstructor();
        if (\is_null($constructor) || $constructor->getNumberOfRequiredParameters() == 0) {
            return $item->newInstance();
        }
        $params = [];
        foreach ($constructor->getParameters() as $param) {
            if ($type = $param->getType()) {
                $params[] = $this->get($type->getName());
            }
        }
        return $item->newInstanceArgs($params);
    }

    public static function getInstance(array $default = []) {
        if (self::$instance == null) {
            self::$instance = new static($default);
            self::$instance->set(ContainerInterface::class, self::$instance);
        }
        return self::$instance;
    }
}