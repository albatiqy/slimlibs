<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Result;

use Slim\Exception\HttpException;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

abstract class ResultException extends HttpException {

    const CODE = 500;
    const MESSAGE = 'Internal server error.';
    const TITLE = '500 Internal Server Error';
    const DESCRIPTION = 'Unexpected condition encountered preventing server from fulfilling request.';

    protected $data = [];
    protected $errType = 0;

    const ERR_TYPES = [
        'UNAUTHORIZED'  => 0,
        'VALIDATION'  => 2,
        'NOT_EXIST'  => 3
    ];

    abstract protected function init();

    public function __construct(ServerRequestInterface $request, $data = [], $message = '')
    {
        //$request = $request->withHeader('Accept', 'application/json'); // do in error handler
        $this->init();
        if ($message=='') {
            $message = static::MESSAGE;
        }
        parent::__construct($request, $message, static::CODE);
        $this->data = $data;
    }

    public function getData() {
        return (object)$this->data;
    }

    public function getErrType() {
        return $this->errType;
    }
}