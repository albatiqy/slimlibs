<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Result;

abstract class AbstractResult {

    const RES_TYPES = [
        'NO_DATA'=> 1,
        'ROWS'   => 2,
        'DATA'   => 4
    ];

    protected $status = 200;

    public $resType = 0;

    public function getStatus() {
        return $this->status;
    }
}