<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Result\Results;

use Albatiqy\Slimlibs\Result\AbstractResult;

final class Data extends AbstractResult {

    public const STATUS_OK = 200;
    public const STATUS_CREATED = 201;

    public $data = [];

    function __construct($data = [], $status = self::STATUS_OK) {
        $this->status = $status;
        $this->resType = static::RES_TYPES['DATA'];
        $this->data = $data;
    }
}