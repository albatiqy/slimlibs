<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\Result\Results;

use Albatiqy\Slimlibs\Result\AbstractResult;

final class NoData extends AbstractResult {

    public const STATUS_NO_CONTENT = 204;

    function __construct() {
        $this->status = self::STATUS_NO_CONTENT;
        $this->resType = static::RES_TYPES['NO_DATA'];
    }
}