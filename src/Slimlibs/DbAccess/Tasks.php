<?php declare (strict_types = 1);
namespace Albatiqy\Slimlibs\DbAccess;

use Albatiqy\Slimlibs\Services\Database\MySqlDbAccess;

final class Tasks extends MySqlDbAccess {

    protected const TABLE_NAME = 'app_tasks a';
    protected const PRIMARY_KEY = 'id';
    protected const ENTITY_NAME = 'task';
    protected const COLUMN_DEFS = [
        'id' => ['db' => 'a.id', 'type' => self::TYPE_NUMERIC],
        'task' => ['db' => 'a.task', 'type' => self::TYPE_STRING],
        'data' => ['db' => 'a.data', 'type' => self::TYPE_STRING],
        'output' => ['db' => 'a.output', 'type' => self::TYPE_STRING],
        'logs' => ['db' => 'a.logs', 'type' => self::TYPE_STRING],
        'state' => ['db' => 'a.state', 'type' => self::TYPE_NUMERIC]
    ];

    protected function getRules($opr) {
        if ($opr == self::RULE_CREATE) {
            return [
                'task' => ['required'],
                'data' => ['required']
            ];
        }
        return [
        ];
    }

    public function remains() {
        $db = $this->db();
        $table = self::TABLE_NAME;
        $sql = "select a.* from $table where a.state=0 order by a.id limit 0,5";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}