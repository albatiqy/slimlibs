<?php declare (strict_types = 1);

//error_reporting(0);
ini_set('display_errors', '0');
date_default_timezone_set('Asia/Jakarta');

$settings = require LIBS_DIR . '/requires/settings.php';
$container = (require LIBS_DIR . '/requires/container.php')($settings);
$app = (require LIBS_DIR . '/requires/app.php')($container);
$app->setBasePath(BASE_PATH);
//$app->getRouteCollector()->setDefaultInvocationStrategy(new Slim\Handlers\Strategies\RequestHandler(true));
$request = $container->request;

(require LIBS_DIR . '/web/route.php')($app);
(require APP_DIR . '/config/middleware.php')($app);

$app->run($request);