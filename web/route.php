<?php declare(strict_types=1);

use Slim\App;
use Slim\Routing\RouteCollectorProxy;
//use Albatiqy\Slimlibs\Middleware\Jwt;

return static function (App $app) {
    $app->group('/api', function (RouteCollectorProxy $group) {
        $group->post('/v0/login', Albatiqy\Slimlibs\Actions\Api\Login0Post::class); // <== jgn di jwt
        require APP_DIR.'/routes/api.php';
    });
    $app->get('/js/modules/globals.js', Albatiqy\Slimlibs\Actions\Web\GlobalsJsGet::class);
    $app->get('/pages/login', Albatiqy\Slimlibs\Actions\Web\LoginSpaGet::class);
    $app->get('/login', Albatiqy\Slimlibs\Actions\Web\LoginMpaGet::class);
    require APP_DIR.'/routes/main.php';
};